#!/usr/bin/python3
import os
import json
from xivo_provd_client import new_provisioning_client

LOCAL_PROVD = "http://"+os.environ["XIVO_HOST"]+":8666/provd"
provd_client = new_provisioning_client(LOCAL_PROVD)
device_manager = provd_client.device_manager()

devices = device_manager.find()
formatted_devices = [
    {
        "id": device.get("id", ""),
        "vendor": device.get("vendor", ""),
        "model": device.get("model", ""),
        "version": device.get("version", ""),
        "plugin": device.get("plugin", ""),
    }
    for device in devices
]
print(json.dumps({"devices": formatted_devices}))
