SELECT (CASE WHEN (staticsip.var_val = '' OR staticsip.var_val IS NULL) THEN FALSE ELSE TRUE END) AS edge_activated,
       EXISTS(SELECT COUNT(queue.name)
              FROM queue
              WHERE queue.name LIKE '%switchboard')                                               AS switchboard_activated,
       (SELECT count(queue.name) FROM queue WHERE queue.name LIKE '%switchboard')                 AS nb_switchboards,
       (SELECT count(usersip.options)
        FROM usersip
        WHERE usersip.options = '{{webrtc,yes}}'
           OR usersip.options = '{{webrtc,ua}}')                                                  AS nb_webrtc_users,
       (SELECT count(id) FROM meetingroom)                                                        AS nb_meetingrooms,
       (SELECT count(id) FROM agentfeatures)                                                      AS nb_agents,
       (SELECT count(id) FROM mediaserver)                                                        AS nb_mds,
       (SELECT count(id) FROM userfeatures WHERE userfeatures.commented != 1) AS nb_users,
       (SELECT CASE WHEN EXISTS ( SELECT 1 FROM information_schema.columns 
 	WHERE table_name = 'userfeatures'  AND column_name = 'mobile_push_token' ) 
	AND EXISTS ( SELECT 1 FROM userfeatures WHERE mobile_push_token IS NOT NULL) THEN true ELSE false END AS mobile_app_activated),
       (SELECT count(distinct(user_id)) AS ldap_user_sync 
	FROM userlabels 
	JOIN labels ON labels.id = userlabels.label_id 
	WHERE labels.display_name = 'synchro_ldap'),
       (SELECT SUM(CASE WHEN mobile_push_token LIKE 'android%' THEN 1 ELSE 0 END) AS nb_mapp_android_users FROM userfeatures),
       (SELECT SUM(CASE WHEN mobile_push_token LIKE 'ios%' THEN 1 ELSE 0 END) AS nb_mapp_ios_users FROM userfeatures)
FROM staticsip
WHERE var_name = 'stunaddr';

