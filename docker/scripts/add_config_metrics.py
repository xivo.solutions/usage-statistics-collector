import os
import sys
from typing import Optional, Union, Dict, List
from line_protocol_parser import parse_line

# Function from telegraf_pyplug lib see: https://github.com/ToolSense/telegraf_pyplug/blob/master/telegraf_pyplug/util.py
def is_str_repr_of_int(string: str) -> bool:

    if string.startswith('-') or string.startswith('+'):
        if string[1:-1].isdigit() and string.endswith('i'):
            return True
    if string[0:-1].isdigit() and string.endswith('i'):
        return True

    return False

# Function from telegraf_pyplug lib see: https://github.com/ToolSense/telegraf_pyplug/blob/master/telegraf_pyplug/main.py
# Modification: [remove of the optionnal parameter 'add_timestamp']
def print_influxdb_format(
        measurement: str,
        fields: Dict[str, Union[str, int, float]],
        tags: Optional[Dict[str, Union[str, int, float]]] = None,
        nano_timestamp: Optional[int] = None
) -> None:

    result: str = f'{measurement}'

    if tags:
        for tag_name, tag_value in tags.items():
            if isinstance(tag_value, str):
                tag_value = tag_value.strip()
                tag_value = tag_value.replace(' ', r'\ ')
                tag_value = tag_value.replace(',', r'\,')
                tag_value = tag_value.replace('=', r'\=')

            result += f',{tag_name}={tag_value}'

    fields_list: List[str] = []
    for field_name, field_value in fields.items():
        if isinstance(field_value, str):
            field_value = field_value.strip()
            if field_value not in ['true', 'True', 'TRUE', 't', 'T', 'false', 'False', 'FALSE', 'f', 'F', ]:
                if not is_str_repr_of_int(field_value):
                    field_value = field_value.replace('"', '\\"')
                    field_value = f'"{field_value}"'

        fields_list.append(f'{field_name}={field_value}')
    result += f' {",".join(fields_list)}'

    if nano_timestamp:
        result += f' {nano_timestamp}'
    print(result)

def read_file_content(file_name):

    try:
        with open(file_name, 'r', encoding='utf-8') as file:
            content = file.read()
        return content
    except FileNotFoundError:
        return "Unknown"
    except IOError:
        return "Error: Problem reading the file."

def main():

    for line in sys.stdin:
        # Parse the original line
        original_line = line.strip()

        parsed_line = parse_line(original_line)
        visio_activated = True if os.getenv('MEETINGROOM_AUTH_DOMAIN') else False
        xivo_version = read_file_content("/usr/share/xivo/XIVO-VERSION")
        xivo_lts = read_file_content("/usr/share/xivo/XIVO-VERSION-LTS")

        # Add additional fields
        parsed_line['fields'].update({'visio_activated': visio_activated, 'xivoversion': xivo_version, 'xivolts': xivo_lts})

        # Output the modified line to stdout for Telegraf to capture
        print_influxdb_format(
            measurement=parsed_line['measurement'],
            tags=parsed_line['tags'],
            fields=parsed_line['fields'],
            nano_timestamp=parsed_line['time']
        )

if __name__ == "__main__":
    main()
