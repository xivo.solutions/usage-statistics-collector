# Usage statistics writer

Related to [#5990](https://projects.xivo.solutions/issues/5990). Using [Telegraf in docker](https://hub.docker.com/_/telegraf). It's an agent that writes usage data in the USM database.
The agent writes usage data into USM database. Currently the writer is tracking login events using `rabbitmq` (through `amqp_consumer` plugin).
It also tracks configuration on the XiVO platflorm using sql queries against `xivo-db` (through `postgresql_extensible` plugin).

### Tips

#### Generate configuration file

To get telegraf default config file with all plugins and their documentation, just run:

```
docker pull telegraf
docker run --rm telegraf telegraf config > telegraf.conf
```

To limit the plugins in the config file use `--input-filter` and `--output-filter`:

```
# In this example we specify input plugins to be cpu, mem, amqp_consumer,
# and postgresql and file as output plugins.
docker run --rm telegraf telegraf --input-filter cpu:mem:amqp_consumer --output-filter postgresql:file config > telegraf.conf
```

#### Configure json_v2 parsing

[This guide](https://github.com/influxdata/telegraf/tree/**master**/plugins/parsers/json_v2) includes examples for parsing different types of JSON messages.


### Troubleshoot

If you need to debug telegraf for some reason.

You can go in the container.

For the outputs you can set the `log_level` to debug to see more info.

You can also launch telegraf by hand with the `debug` flag:

```sh
telegraf --config /etc/telegraf/telegraf.conf --config-directory /etc/telegraf/telegraf.d --debug
```

You can also add the following output to outputs.conf:

```conf
[[outputs.file]]
  files = ["stdout", "/tmp/metrics.out"]
```

Then you can see the metrics:

```bash
tail -f /tmp/metrics.out
```

You should have logs like this:

```
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_mapp_ios_users=0,nb_mapp_android_users=0,nb_users=8,ldap_user_sync=0,switchboard_activated=true,mobile_app_activated=false,nb_mds=2,nb_agents=2,nb_webrtc_users=5,edge_activated=true,nb_meetingrooms=2,nb_switchboards=1,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734600000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_agents=2,switchboard_activated=true,nb_mapp_android_users=0,nb_mds=2,edge_activated=true,nb_mapp_ios_users=0,nb_switchboards=1,mobile_app_activated=false,nb_users=8,nb_meetingrooms=2,ldap_user_sync=0,nb_webrtc_users=5,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734660000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_mapp_android_users=0,nb_agents=2,nb_meetingrooms=2,nb_mds=2,nb_mapp_ios_users=0,nb_users=8,nb_switchboards=1,switchboard_activated=true,ldap_user_sync=0,nb_webrtc_users=5,edge_activated=true,mobile_app_activated=false,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734720000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b mobile_app_activated=false,nb_users=8,nb_mds=2,switchboard_activated=true,nb_agents=2,nb_switchboards=1,edge_activated=true,nb_mapp_ios_users=0,ldap_user_sync=0,nb_meetingrooms=2,nb_webrtc_users=5,nb_mapp_android_users=0,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734780000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_agents=2,nb_mapp_ios_users=0,nb_webrtc_users=5,switchboard_activated=true,nb_switchboards=1,nb_meetingrooms=2,edge_activated=true,nb_mapp_android_users=0,ldap_user_sync=0,mobile_app_activated=false,nb_users=8,nb_mds=2,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734840000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b mobile_app_activated=false,nb_agents=2,nb_meetingrooms=2,nb_mapp_ios_users=0,nb_mds=2,nb_webrtc_users=5,switchboard_activated=true,ldap_user_sync=0,nb_switchboards=1,edge_activated=true,nb_mapp_android_users=0,nb_users=8,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734900000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_mapp_ios_users=0,ldap_user_sync=0,mobile_app_activated=false,edge_activated=true,nb_agents=2,nb_switchboards=1,nb_meetingrooms=2,nb_webrtc_users=5,switchboard_activated=true,nb_mds=2,nb_mapp_android_users=0,nb_users=8,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726734960000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_users=8,switchboard_activated=true,ldap_user_sync=0,nb_mds=2,nb_mapp_ios_users=0,nb_mapp_android_users=0,mobile_app_activated=false,nb_meetingrooms=2,nb_webrtc_users=5,edge_activated=true,nb_agents=2,nb_switchboards=1,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735020000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b mobile_app_activated=false,nb_mds=2,nb_webrtc_users=5,switchboard_activated=true,edge_activated=true,ldap_user_sync=0,nb_mapp_ios_users=0,nb_mapp_android_users=0,nb_users=8,nb_meetingrooms=2,nb_agents=2,nb_switchboards=1,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735080000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b mobile_app_activated=false,nb_mds=2,nb_webrtc_users=5,nb_users=8,edge_activated=true,ldap_user_sync=0,nb_mapp_android_users=0,nb_agents=2,nb_meetingrooms=2,nb_mapp_ios_users=0,nb_switchboards=1,switchboard_activated=true,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735140000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b ldap_user_sync=0,nb_switchboards=1,edge_activated=true,nb_webrtc_users=5,switchboard_activated=true,nb_mapp_android_users=0,mobile_app_activated=false,nb_meetingrooms=2,nb_users=8,nb_mds=2,nb_agents=2,nb_mapp_ios_users=0,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735200000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_meetingrooms=2,nb_switchboards=1,nb_mapp_android_users=0,ldap_user_sync=0,switchboard_activated=true,edge_activated=true,nb_mapp_ios_users=0,nb_users=8,mobile_app_activated=false,nb_agents=2,nb_mds=2,nb_webrtc_users=5,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735260000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b mobile_app_activated=false,nb_agents=2,nb_mds=2,nb_switchboards=1,nb_mapp_android_users=0,nb_users=8,nb_webrtc_users=5,switchboard_activated=true,nb_mapp_ios_users=0,ldap_user_sync=0,nb_meetingrooms=2,edge_activated=true,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735320000000000
config_xivo,db=postgres,server=host\=172.17.0.1\ user\=stats\ dbname\=asterisk,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b nb_mapp_ios_users=0,nb_webrtc_users=5,nb_switchboards=1,ldap_user_sync=0,nb_meetingrooms=2,mobile_app_activated=false,nb_agents=2,nb_users=8,nb_mds=2,switchboard_activated=true,edge_activated=true,nb_mapp_android_users=0,visio_activated=false,xivoversion="2024.05.03",xivolts="Maia" 1726735380000000000
config_devices,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b id="3337b7ec31a9497191320d3be7daf8f9",vendor="Yealink",model="T54W",version="96.85.0.5",plugin="xivo-yealink-v85" 1726735440000000000
config_devices,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b id="b876ca40cb3640d5a2dc456454936a51",vendor="Snom",model="715",version="10.1.46.16",plugin="xivo-snom-10.1.46.16" 1726735440000000000
config_devices,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b id="8ce5c2344a434999b4e96adf9c546ac9",vendor="Snom",model="D735",version="10.1.46.16",plugin="xivo-snom-10.1.46.16" 1726735440000000000
config_devices,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b id="3337b7ec31a9497191320d3be7daf8f9",vendor="Yealink",model="T54W",version="96.85.0.5",plugin="xivo-yealink-v85" 1726735500000000000
config_devices,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b id="b876ca40cb3640d5a2dc456454936a51",vendor="Snom",model="715",version="10.1.46.16",plugin="xivo-snom-10.1.46.16" 1726735500000000000
config_devices,xivo_uuid=4e288243-ee47-4bc7-91f5-5eca844ab05b id="8ce5c2344a434999b4e96adf9c546ac9",vendor="Snom",model="D735",version="10.1.46.16",plugin="xivo-snom-10.1.46.16" 1726735500000000000
```

See https://docs.influxdata.com/telegraf/v1/configure_plugins/troubleshoot/
